var express = require('express');//присваивает модуль express переменной express
var app = express();//инициализируем express и называем его переменной: app
var passport   = require('passport')//импортируем модуль passport для обработки аутентификации
var session    = require('express-session')//импортируем модуль express-session для обработки аутентификации
var bodyParser = require('body-parser')//импортируем модуль, который извлекает всю часть тела входящего запроса и предоставляет его в формате JSON
var env = require('dotenv')//импортируем модуль dot-env для обработки переменных среды
var exphbs = require('express-handlebars')//импортирование модуля представлений

var models = require("./app/models");//импорт моделей 
var authRoute = require('./app/routes/auth.js')(app,passport); //роуты, удалить после переноса на фронт

//load passport strategies
require('./app/config/passport/passport.js')(passport, models.user);

 //For BodyParser инициализируем bodyParser для возможности использования синтаксического анализатора текста
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// For Passport инициализируем passport, express session и passport session и добавляем их как middleware
app.use(session({ secret: 'keyboard cat',resave: true, saveUninitialized:true})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
//For Handlebars
app.set('views', './app/views/')

app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');






//Sync Database
models.sequelize.sync().then(function() {
    console.log('Nice! Database looks fine')
}).catch(function(err) {
    console.log(err, "Something went wrong with the Database Update!")
});







app.get('/', function(req, res) {//вызываем функцию маршрутизации express, если будет выполнен GET запрос к адресу «/»
    res.send('Welcome to Passport with Sequelize');//Отправка результата гет запроса
});
 
 
app.listen(5000, function(err) {//подключаем приложение к порту 5000
    if (!err)
        console.log("Site is live");
    else console.log(err)
});